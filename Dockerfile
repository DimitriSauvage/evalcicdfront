FROM node:latest AS build-env

#Get packages
WORKDIR /app
COPY package.json .
RUN yarn install 

#Build
COPY . /app
RUN yarn global add @angular/cli@~10.0.0
RUN yarn build:prod


# Build runtime image
FROM nginx:stable-alpine
WORKDIR /app
COPY --from=build-env /app/dist/evalcicdfront /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

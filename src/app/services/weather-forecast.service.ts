import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { WeatherForecast } from '../models/WeatherForecast.model';

@Injectable({
  providedIn: 'root',
})
export class WeatherForecastService {
  constructor(private httpClient: HttpClient) {}

  getWeatherForecasts = (): Observable<WeatherForecast[]> =>
    this.httpClient.get<WeatherForecast[]>(
      `${environment.apiUrl}/weatherForecast`
    );
}

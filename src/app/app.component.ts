import { Component } from '@angular/core';
import { asapScheduler, Observable, scheduled } from 'rxjs';
import { WeatherForecast } from './models/WeatherForecast.model';
import { WeatherForecastService } from './services/weather-forecast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'evalcicdfront';
  weatherForecasts: Observable<WeatherForecast[]> = scheduled([], asapScheduler);

  constructor(weatherForecastService: WeatherForecastService) {
    this.weatherForecasts = weatherForecastService.getWeatherForecasts();
  }
}
